#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 14 09:07:08 2018

@author: tina
"""

from setuptools import setup

setup(name='transcoder',
      version='0.2',
      description='VIAA transcoder.',
      long_description='VIAA transcoder.',
      classifiers=[
        'Development Status :: 3 - rc1',
        'Intended Audience :: Developers',
        'License :: MIT 2017 VIAA',
        'Programming Language :: Python :: 3.5',
        'Topic :: ffmpeg',
      ],
      keywords='ffmpeg transcode module',
      author='Tina Cochet',
      author_email='tina.cochet@viaa.be',
      license='MIT 2017 VIAA',
      packages=['transcoder'],
      install_requires=[
              'ffmpy',
              'pyjq',
      ],
      scripts=['transcoder/scene_detect.sh',
               'transcoder/crop_borders.sh',
               ],
#      entry_points={
#          'console_scripts': ['transcoder=transcoder.command_line:main'],
#      },
      include_package_data=True,
      zip_safe=False)
