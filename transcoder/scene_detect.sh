#! /bin/bash
#set -x
ffmpeg -i $1 -vf select='gt(scene\,0.4),showinfo,scale=-1:96' -vsync vfr  frame%d.png   2> ffout
grep showinfo ffout |  grep  pts_time:[0-9.]* -o | cut -d ':' -f 2
#rm ffout
#-vf select='gt(scene\,0.4),showinfo,scale=-1:96' -vsync vfr  frame%d.png   2> ffout
