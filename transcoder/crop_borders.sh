#!/usr/bin/env bash
#set -x
t1=$(ffmpeg -ss 00:00:05 -i  "$1" -t 20 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }'   \
      | sort | uniq -c | sort -n | tail -1 | sed -e 's/^[ \t]*//' | cut -d ' ' -f 2 )
t2=$(ffmpeg -ss 00:00:20 -i  "$1" -t 20 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' \
| sort | uniq -c | sort -n | tail -1 | sed -e 's/^[ \t]*//' | cut -d ' ' -f 2)
t3=$(ffmpeg -ss 00:00:40 -i  "$1" -t 10 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' \
| sort | uniq -c | sort -n | tail -1 | sed -e 's/^[ \t]*//' | cut -d ' ' -f 2)

if [[ "$t1" == "$t2" && "$t1" == "$t3"  ]];then
  #next line to test for negative cropping
  echo "$t1"  | egrep -i  "*-" > /dev/null
  if [[   $? != 0 ]];then
        echo   "$t1"
	exit 0
    fi
    echo ""
  exit 0
else
  if [[ "$t1" == "$t2" ]] ;then
    echo "$t1" | egrep -i  "*-" 2> /dev/null
    if [[  $? != 0 ]];then
        echo  "$t1"
	exit 0
    fi
    echo ""
    exit 0
  fi
  if [[ "$t2" == "$t3" ]] ;then
	 echo "$t2" | egrep -i  "*-" > /dev/null
    if [[   $? != 0 ]];then
    	echo "$t2"
	exit 0
    fi
    echo ""
    exit 0
  fi
  if [[ "$t1" == "$t3" ]] ;then

   echo "$t1" | egrep -i  "*-"
    if [[  $? != 0 ]];then
        echo "$t1"
	exit 0
    fi
    echo ""
    exit 0
  fi

fi
echo ""
