#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 4 17:43:26 2017

@author: tina
"""
import subprocess
import shlex
import json
import pyjq
import logging
import ffmpy
import time
from subprocess import PIPE
import os

"""
USAGE:
     smartTranscode('/home/tina/Videos/JP2K.mxf',profile='ffv1',
                    outfile='ffv1',
                    crop=False,destpath='/tmp',
                    duration=10,
                    start_tc='00:00:00',
                    aspect=None,crop_top=False,leftchan=False).transcode()
TODO: 
    clean up
    add optional noise filter : -filter:v hqdn3d=4.0:3.0:6.0:4.5
    consider -qscale 4 for prores to speedup, QC needed (min speedup 130%)

"""
LOG_FORMAT = ('[%(asctime)-15s: %(levelname)s/%(name)s] %(funcName)s ' +\
                '%(lineno)-4d: %(message)s')
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
LOGGER = logging.getLogger(__name__)
LOG_FORMAT = ('[%(asctime)-15s: %(levelname)s/%(name)s] %(funcName)s ' +\
                '%(lineno)-4d: %(message)s')
logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)


def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        LOGGER.info('%s function took %0.3f seconds' % (f.__name__,
                                                        (time2-time1)))
        return ret
    return wrap


class smartTranscode(object):
    def __init__(self, infile, profile, outfile=None, start_tc='00:00:00',
                 destpath='/tmp', crop=False, o_width=-1, o_height=0,
                 duration=None, auto_scale=True, aspect=None, threads=1,
                 downmix=False,audio_as_is=False,crop_top=False, 
                 leftchan=False,rightchan=False,audio_copy_streams=False, 
                 audio_pcm24=False):
        self.infile = infile
        self.outfile = outfile
        self.profile = profile
        self.start_tc = start_tc
        self.duration = duration
        self.destpath = destpath
        self.o_width = o_width
        self.o_height = o_height
        self.crop = crop
        self.auto_scale = auto_scale
        self.aspect = aspect
        self.threads = threads
        self.downmix = downmix
        self.topts = ''
        self.crop_top =crop_top
        self.leftchan = leftchan
        self.rightchan = rightchan
        self.pcm24=audio_pcm24
        self.audio_copy_streams = audio_copy_streams
        if self.profile == 'mp4_pal_4_3' or  self.profile == 'mp4_pal_16_9':
            self.auto_scale = False
        if self.profile == 'avo' or self.profile == 'avo_crop_top':
            self.auto_scale = False
        if self.profile == 'ffv1':
            self.audio_copy_streams = True
            self.auto_scale = False
            self.pcm24=True
        if  self.profile == 'D10':
            self.audio_copy_streams = True
            self.auto_scale = False
            self.pcm24=True
        if self.profile == 'XDCAM_HD422':
            self.audio_copy_streams = True
            self.auto_scale = False
            self.pcm24=True
        if self.profile == 'FC_PRO':
            self.audio_copy_streams = True
            self.auto_scale = False
            self.pcm24=True
                
        cvopts = makeVCodecOpts(self.infile, profile=self.profile,
                                threads=self.threads)[0]
        self.ext = makeVCodecOpts(self.infile, profile=self.profile,
                                  threads=self.threads)[1]
        vfopts = makeVideofilter(self.infile, crop=self.crop,
                                 output_height=self.o_height,
                                 output_width=o_width,
                                 aspect=self.aspect,
                                 auto_scale=self.auto_scale,
                                 crop_top=self.crop_top)
        aopts = makeAudioFilter(self.infile,
                                downmix=self.downmix,
                                leftchannels=self.leftchan,
                                rightchannels=self.rightchan,
                                copy_streams=self.audio_copy_streams,
                                pcm_24=self.pcm24)


        self.topts = vfopts + cvopts + aopts

    def transcode(self):
        LOGGER.info('File {} processing, please be patient '
                    .format(self.infile) +
                    'start_tc: {} '.format(self.start_tc) +
                    'duration: {} '.format(self.duration))
        if self.duration is not None:
            opts = self.topts + ' -t ' + str(self.duration)
        else:
            opts = self.topts

        @timing
        def ffmpeg_work():
            if self.outfile is not None:
                basename = os.path.basename(self.outfile)
                basename, baseext = os.path.splitext(basename)

            else:
                basename = os.path.basename(self.infile)
                basename, baseext = os.path.splitext(basename)
            if not self.destpath.endswith('/'):
                self.destpath = self.destpath + '/'
            outfile = self.destpath + basename + self.ext
            LOGGER.info('Processing OUTPUT file: {}'.format(str(outfile)))
            ff = ffmpy.FFmpeg(
                inputs={self.infile: '-ss ' + str(self.start_tc)},
                outputs={outfile: opts}
            )
            try:
                LOGGER.info(ff.cmd)
                outfile=ff.cmd.split(' ')[-1]
                LOGGER.info('OUTFILE: {}'.format(outfile))
                self.outfile =outfile
                stdout, stderr = ff.run(stdout=PIPE, stderr=PIPE)
            except Exception as e:
                LOGGER.error(str(e))
#                exit()
                return None
        ffmpeg_work()
        return str(self.outfile)


def makeVideofilter(pathToInputVideo, crop=False, output_height=-1,
                    output_width=0, aspect=None, auto_scale=True, 
                    crop_top=False):
    cmd = "ffprobe -v quiet -print_format json -show_streams "
    args = shlex.split(cmd)
    pathToInputVideo=os.path.abspath(pathToInputVideo)
    args.append(pathToInputVideo)
    # run the ffprobe process, decode stdout into utf-8 & convert to JSON
    ffprobeOutput = subprocess.check_output(args).decode('utf-8')
    ffprobeOutput = json.loads(ffprobeOutput)
    v = []
    v = pyjq.all('.streams[] | select(.codec_type == "video")', ffprobeOutput)
    if v is not '[]':
        # LOGGER.info('Found videostream:' + str(v))
        height = pyjq.first('.[].height', v)
        width = pyjq.first('.[].width', v)
        LOGGER.info('Input Resolution: ' + str(width) + 'x' + str(height))
        yadif = ''
        vf = ''
        try:
            field_order = pyjq.first('.[].field_order', v)
            LOGGER.info('Field Order {}'.format(field_order))
            if field_order == 'tt' or field_order == 'tb':
                LOGGER.info('Field order: Top Field First')
                yadif = 'yadif=0:-1:0'
                LOGGER.info('yadif filter: {} '.format(yadif))
            if field_order == 'bb' or field_order == 'bt':
                interlace = 'tinterlace=mode=merge,'
                LOGGER.info('Field order: Bottem Field First')
                yadif = 'yadif=0:-1:0'
                LOGGER.info('yadif filter: {} '.format(yadif))
        except KeyError as e:
            LOGGER.warning('no field info, will set to progressive, error:' +
                           str(e))
            pass
        sample_aspect_ratio = pyjq.first('.[].sample_aspect_ratio', v)
        LOGGER.info('SAR: ' + sample_aspect_ratio)
        t = sample_aspect_ratio.split(':', 1)[0]
        n = sample_aspect_ratio.split(':', 1)[1]
        time_base = pyjq.first('.[].time_base', v)
        if int(t)/int(n) < 1 and time_base == '1/50':
            height = int(height) * 2
            interlace = 'tinterlace=mode=merge,'
            LOGGER.info('merging fields to progressive 50i to 25p')
            vf = interlace  + yadif 
        if int(t)/int(n) >= 1:
            LOGGER.info('tinterlaced is false')
            vf = yadif
        display_aspect_ratio = pyjq.first('.[].display_aspect_ratio', v)
        h = int(display_aspect_ratio.split(':', 1)[0])
        w = int(display_aspect_ratio.split(':', 1)[1])
        input_aratio = h / w
        LOGGER.info('Calculated input aspectratio: {}'.format(input_aratio))
        LOGGER.info('DAR: ' + display_aspect_ratio)
        vcodec_name = ffprobeOutput['streams'][0]['codec_name']
        LOGGER.info('INPUT video_codec:' + vcodec_name)
    if crop:
        cmd = "crop_borders.sh"
        args = shlex.split(cmd)
        args.append(pathToInputVideo)
        crop = subprocess.check_output(args).decode('utf-8')
        c = crop[:-1]
        LOGGER.info('cropfilter: {} '.format(c))
        if c != '':
            print(vf)
            vf = c + ',' + vf
            # enabele autoscale
            auto_scale = True
    if crop_top:
        vf = ' crop="iw-16:trunc(ih/2-32/2)*2:8:32,"' + vf
    if auto_scale:

        if output_height == -1:
            LOGGER.info('autoscale,fixed from input height: {}'
                        .format(output_height))

            scale = "scale=trunc(oh*a/2)*2:%s" % (height)

        elif output_height == 0:

            LOGGER.info('autoscale,fixed from requested height: {}'
                        .format(output_height))
            scale = "scale=" + str(width) + ':' + str(height)
        elif output_height >= 32:
            scale = "scale=-2:%s" % (output_height)

        if output_width >= 32:
            LOGGER.info('autoscale,fixed from requested width: {} '
                        .format(str(output_width)))
            LOGGER.warning('This overwrite output_height')
            scale = "scale=%s:-2" % (output_width)
        if output_height >= 32 and output_width >= 32:
            scale= "scale={}:{}".format(output_width,output_height)
        LOGGER.info(vf)

        if yadif != '':
            vf = vf + ',' + scale
        else:
            vf = vf + scale

    if vf != '':
        vf = ' -vf ' + vf
    if aspect is not None:
        if aspect == 'auto':
            aspect = ' -aspect {}'.format(display_aspect_ratio)
        else:
            aspect = ' -aspect {}'.format(aspect)
        vf = vf + aspect
    # Final video filter

    LOGGER.info('video filter: ' + str(vf))

    return vf


def makeAudioFilter(pathToInputVideo, downmix=False, leftchannels=False,
                    rightchannels=False,copy_streams=False,pcm_24=False):
    if copy_streams is False:
        cmd = "ffprobe -v quiet -print_format json -show_streams "
        args = shlex.split(cmd)
        args.append(pathToInputVideo)
        afilter = ''
        ffprobeOutput = subprocess.check_output(args).decode('utf-8')
        ffprobeOutput = json.loads(ffprobeOutput)
        j = pyjq.all('.streams[].codec_type', ffprobeOutput)
        j.remove('video')
        nr_audio_tracks = len(j)
        LOGGER.info('Found {}, nr of audio tracks'.format(nr_audio_tracks))
        maps = ''
        nr = nr_audio_tracks
        if int(nr) > 1 and downmix is True:
            LOGGER.info('Downmixing of audio requested')
            for x in range(0, nr):
                map = '[0:' + 'a:' + str(x) + ']'
                maps = maps + str(map)
            afilter = ' -filter_complex ' + '"' + maps + ' ' +\
                      'amerge=inputs={}'.format(nr) + ' [a]' + '"' +\
                      ' -map 0:v -map "[a]"?'
            LOGGER.info('Downmix filter ' + str(afilter))
        if leftchannels is True and downmix is False:
            afilter = ' -map 0 -filter_complex "pan=stereo|FL=c0:FR=c0"  '
        if rightchannels is True and downmix is False:
            afilter = ' -map 0 -filter_complex "pan=stereo|FL=c1:FR=c1" '
        if leftchannels is False and rightchannels is False and downmix is False:
            afilter = ' -map 0:v -map 0:a -filter_complex channelsplit=channel_layout=stereo'
            LOGGER.info('No Audio filter given, mapping all streams {} '
                        .format(str(afilter)))
       
        # calculate how many stereo streams are needed
        if nr is 2 or nr is 1 and downmix is False:
            afilter = afilter + ' -ac 1'
        if nr > 3 and nr < 5 and downmix is False:
            afilter = afilter + ' -ac 2'
        if nr > 4 and nr < 7 and downmix is False:
            afilter = afilter + ' -ac 3'
        if nr > 7 and downmix is False:
            afilter = afilter + ' -ac 4'
        LOGGER.info('Audio filter: ' + str(afilter))
        
    else:
        # copy all audio streams as is 
        # TODO fix use -c:a copy if format is not avo
        afilter = ' -map 0:v:0 -map 0:a:0'
    if not pcm_24:
        afilter = afilter + ' -codec:a libfdk_aac'
    else:
        afilter = afilter + ' -c:a pcm_s24le'
    LOGGER.info('audio filter: {}'.format(afilter))
    return afilter


def makeVCodecOpts(pathToInputVideo, profile='mp4', threads=1):
    if profile == 'mp4':
        ext = '.mp4'
        opts = ' -hide_banner -loglevel warning -y -codec:v libx264' +\
               ' -profile:v high' + \
               ' -preset fast -b:v 1400k -maxrate 1500k' +\
               ' -sn -dn -write_tmcd 0 -movflags faststart ' + \
               ' -bufsize 2000k -pix_fmt yuv420p -threads {}'.format(threads)
        LOGGER.info('mp4 opts: ' + str(opts))
    elif profile == 'prores':
        ext = '.mov'
        opts = ' -hide_banner -loglevel warning -y -c:v prores_ks' +\
               ' -profile:v 3' + \
               ' -q:v 0 -threads {}'.format(threads)
        LOGGER.info('Prores opts:' + str(opts))
    elif profile == 'prores_default':
        ext = '.mov'
        opts = ' -hide_banner -loglevel warning -y -c:v prores_ks' +\
               ' -profile:v 0' + \
               ' -q:v 0 -threads {} '.format(threads)
        LOGGER.info('Prores_default opts: ' + str(opts))
    elif profile == 'ffv1':
        ext = '.mkv'
        LOGGER.warning('this profile only converts video to ffv1'
                       ', no audio filters')
        opts = ' -hide_banner -loglevel info -y -c:v ffv1 ' + \
               ' -level 3 -g 1 -slicecrc 1 -slices 16 ' + \
               ' -threads {} -ac 4'.format(threads)
        LOGGER.info('FFV1 opts: ' + str(opts))
    elif profile == 'mp4_pal_4_3':
        ext = '.mp4'
        opts = ' -vf crop=iw-16:ih:8:0' +\
               ',scale=768:576,setdar=4:3,setsar=1:1' +\
               ' -sn -dn -movflags faststart ' + \
               ' -hide_banner -loglevel info -y -codec:v libx264' +\
               ' -profile:v high -preset fast -b:v 1400k -maxrate 1500k' +\
               ' -bufsize 2000k -pix_fmt yuv420p' +\
               ' -threads {} '.format(threads)
    elif profile == 'mp4_pal_16_9':
        ext = '.mp4'
        opts = ' -vf crop=iw-16:ih:8:0' +\
               ',scale=1024:576,setdar=16:9,setsar=1:1' +\
               ' -sn -dn -movflags faststart ' + \
               ' -hide_banner -loglevel info -y -codec:v libx264' +\
               ' -profile:v high -preset fast -b:v 1400k -maxrate 1500k' +\
               ' -bufsize 2000k -pix_fmt yuv420p -write_tmcd 0 ' +\
               ' -threads {} '.format(threads)


        LOGGER.info('PAL opts: ' + str(opts))
    # TODO check if crop needs rescale or just crop top keep aspect as is !!
    elif profile == 'avo_crop_top':
        ext = '.mp4'
        opts = ' -hide_banner -y -loglevel error -c:v libx264' +\
               ' -preset ultrafast -crf 15 -c:a libfdk_aac -ar 48000' +\
               ' -b:a 192k -vf crop="iw:trunc(ih/2-32/2)*2:0:32"' +\
               ' -sn -dn -write_tmcd 0' + \
               ' -movflags faststart '
    elif profile == 'avo':
        ext = '.mp4'
        opts = ' -hide_banner -y -loglevel error -c:v libx264' +\
               ' -preset ultrafast -crf 15 -c:a libfdk_aac -ar 48000 ' +\
               ' -b:a 192k -movflags faststart ' +\
               ' -sn -dn -write_tmcd 0'
    elif profile == 'XDCAM_HD422':
        ext ='.mxf'
        opts = ' -y -hide_banner -y -loglevel info -vf scale=720:576 '+\
               ' -c:v mpeg2video -b:v 50M -pix_fmt yuv422p'+\
               ' -ar 48000 -ac 4 -standard PAL -threads {} '.format(threads)
    elif profile == 'lossless_mov':
        ext='.mov'
        opts=' -codec:v v210 -pix_fmt yuv422p10le  -threads {} '.format(threads)
    elif profile =='D10':
        ext='.mxf'
        opts=' -hide_banner -y -loglevel info -y -vf scale=720:576 '+\
             ' -c:v mpeg2video -r 25 -pix_fmt yuv422p' +\
             ' -minrate 50000k -maxrate 50000k -b:v 50000k'+\
             ' -intra -flags +ildct+low_delay -intra_vlc 1 -non_linear_quant 1'+\
             ' -ps 1 -qmin 1 -qmax 3 -top 1 -dc 10 -bufsize 2000000'+\
             ' -rc_init_occupancy 2000000 -rc_max_vbv_use 1'+\
             ' -ar 48000 -ac 4 -standard PAL  '
    elif profile =='FC_PRO':
        ext='.mov'
        opts=' -vf scale=720:576 -c:v rawvideo -pix_fmt uyvy422 -vtag 2vuy '+\
             ' -ar 48000 -ac 4 -standard PAL -threads {} '.format(threads)
    # Fallback to mp4, disabled
#    else:
#        ext = '.mp4'
#        opts = '-hide_banner -loglevel warning -y -codec:v libx264' +\
#               ' -profile:v high' + \
#               ' -preset fast -b:v 1400k -maxrate 1500k' +\
#               ' -sn -dn' + \
#               ' -bufsize 2000k -pix_fmt yuv420p -threads {}'.format(threads)
#        LOGGER.info('defaulting to mp4 opts: ' + str(opts))
    return opts, ext
#smartTranscode('/home/tina/Videos/browse.mp4',profile='mp4_pal_4_3',
#                    outfile='left',
#                    crop=False,destpath='/tmp',
#                    duration=10,
#                    start_tc='00:00:00',
#                    threads=4,
#                    downmix=False,
#                    aspect=None,crop_top=False).transcode()